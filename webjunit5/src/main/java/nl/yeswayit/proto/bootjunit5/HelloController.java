package nl.yeswayit.proto.bootjunit5;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.PostConstruct;
import java.util.Map;


@Slf4j
@Controller
public class HelloController {
    @Autowired
    private ApplicationContext applicationContext;


    @Value("${welcome.message:test}")
    private String message = "Hello World";

    @PostConstruct
    public void setup(){
        log.info("HelloController is loaded: \nApp name = {}", applicationContext.getApplicationName());

    }

    @RequestMapping(value="/", method = RequestMethod.GET)
    public ModelAndView showIntro(Map<String, Object> model){
        model.put("message", this.message);
        return  new ModelAndView("start", model);
    }
}
