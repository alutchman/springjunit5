# Vereisten
* Spring Boot version naar 2.4.2, waardoor boot niet meer werkt
* Werk niet in Intellij maar Moet gebruik maken van Tomcat op machine
* Werkt NIET met Java 8 en is ontwikkeld op Java 11
* Als build faalt:   mvn dependency:purge-local-repository


# Laatste Upgrades
* 1 Februari 2021

Instructies voor deze  [markdown file](https://nl.wikipedia.org/wiki/Markdown)

# To run 
* java -jar webunit5.war
* mvn spring-boot:run